# Snake

## Overview
Just a simple snake clone I made using Processing 3 libraries. I also added scene functionality to the app just to see how that worked. 

## What I Learned
I felt inspired to make this becuase I thought I might be able to use Linked Lists in a creative way to 
create the chaining of the tail. That ended up not working because there was no easy way to recognise a 
part of the snake in the middle of the snake so he could clip into himself. 
It was entirely simpler to just make a normal array and store the body objects in that and just do a quick check if the array contained a square.

## Usage
Use arrow keys to control the direction the snake goes. 
